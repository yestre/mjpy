MJPY -- A Media Jukebox in Python
=================================

mjpy is a python program that mimics the popular "YAMJ" written in Java, that features a simpler logic and makes
it easier (for myself :-)) to add functions and fix data issues.

Highlights:

* built-in SQLITE database; separate movie metadata from media files, therefore make the movie metadata reusable for different media
files.
* automatic scraper for IMDB data and poster images.
* command line to fix movie and media data.
* Jinja based template for HTML file, instead of using XML and XSLT.

I only tested the code on Mac OS X. The code should easily work on a Linux box.

Pre-requisites
--------------

* jinja2
* numpy (for image processing)
* PIL
* requests
* lxml

Setup
-----

Currently no effort made to package it.

Create a directory under top-level called "mjdb" to hold the database and other metadata (such as poster images).

Then run from the top directory:

    python mj.py ...

Usage
-----

**First step** is to build your media database. Typically all you need to do is either adding a new, single file:

    python mj.py single --file /Users/x/Movies/DR.No.mp4

Or, process all files in a directory:

    python mj.py scan --indir /Users/x/Movies

You typically end up with many media files and their associated movie metadata being automatically recognized
by the system and added to the database.
However, there could be failed cases, which will involve manual fixing.

You add a new movie entry to DB (independent of a media file) by doing this:

    python mj.py db --id imdb-tt0087843 --op addmovie

Note in the above, the id's format is always imdb-<imdb-id>. We currently only support pulling data from IMDB. We may add other data
sites in future.

You can fix fields in a movie entry (check schema.sql for movie field names):

    python mj.py db --op fixmovie --fld image_path --val http://i.yai.bz/Assets/65/827/l_p1011982765.jpg --id imdb-tt1522814

This manually add a movie poster (suppose IMDB site doesn't have any movie image).

Fix a wrongly labeled (wrong movie id) media entry:
    
    python mj.py db --op fixmedia --spath '/Volumes/WD-1T/movies/Batman.Begins.2005.BluRay.720p.x264.mkv' --id imdb-tt0372784

Now the **jukebox generation step**.

Generate thumbnail image for a movie (suppose we just added a poster image from the above step):

    python mj.py gen --poster imdb-tt1522814 --odir /Users/x/mjpy/jukebox

--odir specifies the directory to hold all generated code.

Generate thumbnail images for all movies in the database:

    python mj.py gen --poster all --odir /Users/x/mjpy/jukebox

Generate all HTML code, but skips poster thumbnails generation:

    python mj.py gen --html --odir /Users/x/mjpy/jukebox --sprefix /Volumes/WD-1T/movies --dprefix file:///opt/sybhttpd/localhost.drives/USB_DRIVE_A-1/movies


