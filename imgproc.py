import numpy
from PIL import Image, ImageColor, ImageDraw

def find_coeffs(pa, pb):
    matrix = []
    for p1, p2 in zip(pa, pb):
        matrix.append([p1[0], p1[1], 1, 0, 0, 0, -p2[0]*p1[0], -p2[0]*p1[1]])
        matrix.append([0, 0, 0, p1[0], p1[1], 1, -p2[1]*p1[0], -p2[1]*p1[1]])

    A = numpy.matrix(matrix, dtype=numpy.float)
    B = numpy.array(pb).reshape(8)

    res = numpy.dot(numpy.linalg.inv(A.T * A) * A.T, B)
    return numpy.array(res).reshape(8)

def perspect_shrink(img, shrink):
    '''
    do the perspective transform.
    shrink: the shrink ratio.
    '''
    width, height = img.size
    d = int(height * shrink)
    coeffs = find_coeffs(
        [(0, 0), (width, d), (width, height-d), (0, height)],
        [(0, 0), (width, 0), (width, height), (0, height)])

    img = img.transform((width, height), Image.PERSPECTIVE, coeffs, Image.NEAREST)
    return img

def make_effect(im, bgcolor="#000000", amount=0.4, opacity=0.6, shrink=0.03):
    """ Returns the supplied PIL Image (im) with a reflection and perspective shrink effect.

    bgcolor  The background color of the reflection gradient
    amount   The height of the reflection as a percentage of the orignal image
    opacity  The initial opacity of the reflection gradient

    Originally written for the Photologue image management system for Django
    and Based on the original concept by Bernd Schlapsi

    """
    # make round corner
    im = roundcorner(im)

    # convert bgcolor string to rgb value
    background_color = ImageColor.getrgb(bgcolor)

    # copy orignial image and flip the orientation
    reflection = im.copy().transpose(Image.FLIP_TOP_BOTTOM)

    # create a new image filled with the bgcolor the same size
    background = Image.new("RGB", im.size, background_color)

    # calculate our alpha mask
    start = int(255 - (255 * opacity)) # The start of our gradient
    steps = int(255 * amount) # the number of intermedite values
    increment = (255 - start) / float(steps)
    mask = Image.new('L', (1, 255))
    for y in range(255):
        if y < steps:
            val = int(y * increment + start)
        else:
            val = 255
        mask.putpixel((0, y), val)
    alpha_mask = mask.resize(im.size)

    # merge the reflection onto our background color using the alpha mask
    reflection = Image.composite(background, reflection, alpha_mask)
    # make round corner, again
    reflection = roundcorner(reflection)

    # crop the reflection
    reflection_height = int(im.size[1] * amount)
    reflection = reflection.crop((0, 0, im.size[0], reflection_height))

    # create new image sized to hold both the original image and the reflection
    composite = Image.new("RGBA", (im.size[0], im.size[1]+reflection_height+5))

    # paste the orignal image and the reflection into the composite image
    composite.paste(im, (0, 0))
    composite.paste(reflection, (0, im.size[1]+5))

    return perspect_shrink(composite, shrink)

def roundcorner(img, corner_percent=0.095):  #0.138889
    w,h= img.size
    corner_sz = int(h*corner_percent)
    mask = Image.new('L', img.size, 0)
    draw = ImageDraw.Draw(mask)
    # draw two inner rect                                                                                                                       
    draw.rectangle((corner_sz, 0, w-corner_sz, h), fill=255)
    draw.rectangle((0, corner_sz, w, h-corner_sz), fill=255)
    # draw corners                                                                                                                              
    draw.ellipse((0, 0, 2*corner_sz, 2*corner_sz), fill=255)
    draw.ellipse((0, h-2*corner_sz, 2*corner_sz, h), fill=255)
    draw.ellipse((w-2*corner_sz, 0, w, 2*corner_sz), fill=255)
    draw.ellipse((w-2*corner_sz, h-2*corner_sz, w, h), fill=255)
    img.putalpha(mask)
    return img

if __name__ == '__main__':
    import sys
    img = Image.open(sys.argv[1])
    shrink = float(sys.argv[2])
    refl = float(sys.argv[3])
    bg = sys.argv[4]
    img = make_effect(img, amount=refl, bgcolor=bg, shrink=shrink)
    img.save(sys.argv[5])

    # use the following to generate similar cover art for the movie.
    # python imgproc.py infile.jpg 0.03 0.12 '#909090' ofile.png
