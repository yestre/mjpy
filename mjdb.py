import sqlite3

class MJDB:
    stopwords = ['A', 'AN', 'THE', 'OF', 'ON', 'TO', 'AND']
    def __init__(self, dbdir):
        self.conn = sqlite3.connect(dbdir+'/'+'db')
        self.conn.text_factory = lambda x: unicode(x, 'utf-8', 'ignore')
        self.conn.row_factory = sqlite3.Row
        self.cursor = self.conn.cursor()

    def ctitle(self, title):
        uppercase = title.upper()
        return ' '.join([w for w in uppercase.split() if w not in self.stopwords])
            
    def add_movie(self, movie_data):
        '''
        @movie_data is a dict.
        '''
        movie_data['ctitle'] = self.ctitle(movie_data['title'])
        fields_str = ', '.join(movie_data.keys())
        qm_str = ', '.join(['?']*len(movie_data))
        sql = "INSERT INTO movie ({0}) VALUES ({1})".format(fields_str, qm_str)
        self.cursor.execute(sql, movie_data.values())
        self.conn.commit()

    def upd_movie(self, movie_data):
        if movie_data.has_key('title'):
            movie_data['ctitle'] = self.ctitle(movie_data['title'])            
        assign_str = ', '.join(["{0}=?".format(k) for k in movie_data.keys() if k!='id'])
        sql = "UPDATE movie SET {0} WHERE id=?".format(assign_str)
        params = [movie_data[k] for k in movie_data.keys() if k != 'id']+[movie_data['id']]
        self.cursor.execute(sql, params)
        self.conn.commit()

    def get_movie(self, mid, field):
        self.cursor.execute("SELECT {0} FROM movie WHERE id=?".format(field), (mid,))
        row = self.cursor.fetchone()
        return row[0] if row else None

    def add_episode(self, ep_data):
        '''
        add episode and return the row id.
        '''
        fields_str = ', '.join(ep_data.keys())
        qm_str = ', '.join(['?']*len(ep_data))
        sql = "INSERT INTO tv_episode ({0}) VALUES ({1})".format(fields_str, qm_str)
        self.cursor.execute(sql, ep_data.values())
        self.conn.commit()
        self.cursor.execute("SELECT last_insert_rowid() FROM tv_episode")
        row = self.cursor.fetchone()
        return row[0]

    def get_episode(self, mid, season, ep):
        self.cursor.execute("SELECT rowid FROM tv_episode WHERE season=? AND epno=? AND parent=?", (season, ep, mid))
        r = self.cursor.fetchone()
        return r[0] if r else None

    def upd_episode(self, ep_data, parent_id):
        pass

    def add_media(self, media_data):
        fields_str = ', '.join(media_data.keys())
        qm_str = ', '.join(['?']*len(media_data))
        sql = "INSERT INTO media ({0}) VALUES ({1})".format(fields_str, qm_str)
        self.cursor.execute(sql, media_data.values())
        self.conn.commit()

    def upd_media(self, media_data):
        assign_str = ', '.join(["{0}=?".format(k) for k in media_data.keys() if k!='src_path'])
        sql = "UPDATE media SET {0} WHERE src_path=?".format(assign_str)
        params = [media_data[k] for k in media_data.keys() if k != 'src_path']+[media_data['src_path']]
        self.cursor.execute(sql, params)
        self.conn.commit()

    def upsert_media(self, media_data):
        try:
            self.add_media(media_data)
        except:
            self.upd_media(media_data)

    def get_media(self, src_path):
        self.cursor.execute("SELECT * FROM media WHERE src_path=?", (src_path,))
        r = self.cursor.fetchone()
        return r[0] if r else None
    
    def get_media_by_movie(self, mid):
        self.cursor.execute("SELECT media.* FROM media WHERE movie_id=?", (mid,))
        rows = self.cursor.fetchall()
        rows = [dict([(k, row[k]) for k in row.keys()]) for row in rows]
        for row in rows:
            if row['episode_id']:
                self.cursor.execute("SELECT title, season, epno FROM tv_episode WHERE rowid=?", (row['episode_id'],))
                ep = self.cursor.fetchone()
                row['title'] = '{0}.{1} {2}'.format(ep['season'], ep['epno'], ep['title'])
        return rows
        
    def get_rows_as_dict(self, sql):
        self.cursor.execute(sql)
        rows = self.cursor.fetchall()
        return [dict([(k, row[k]) for k in row.keys()]) for row in rows]

    def get_all_media(self):
        return self.get_rows_as_dict("SELECT media.*, movie.* FROM media, movie WHERE media.movie_id=movie.id ORDER BY movie.ctitle")

    def get_all_movies_with_media(self):
        '''
        returns only that movie rows that have an associated media entry.
        '''
        sql = "SELECT DISTINCT movie.* FROM media, movie WHERE media.movie_id=movie.id ORDER BY movie.ctitle"
        return self.get_rows_as_dict(sql)
    
    def get_movies_begin_with_numbers(self):
        sql = "SELECT DISTINCT movie.* FROM media, movie WHERE media.movie_id=movie.id and substr(movie.ctitle, 1, 1)>='0' and substr(movie.ctitle, 1, 1)<='9' ORDER BY movie.ctitle"
        return self.get_rows_as_dict(sql)

    def get_movies_begin_with_letter(self, letter):
        sql = "SELECT DISTINCT movie.* FROM media, movie WHERE media.movie_id=movie.id and substr(movie.ctitle,1,1)='{0}' ORDER BY movie.ctitle".format(letter)
        return self.get_rows_as_dict(sql)

    def get_movies_with_genre(self, genre):
        sql = "SELECT DISTINCT movie.* FROM media, movie WHERE media.movie_id=movie.id and movie.genre like '%{0}%' ORDER BY movie.ctitle".format(genre)
        return self.get_rows_as_dict(sql)

    def get_movies_with_top250(self):
        sql = "SELECT DISTINCT movie.* FROM media, movie WHERE media.movie_id=movie.id and movie.top250>0 ORDER BY movie.ctitle"
        return self.get_rows_as_dict(sql)

    def get_num_movie_by_first_letter(self):
        '''get count of movies by first letter. All movie names beginning with number goes to one bucket.
        '''
        self.cursor.execute("SELECT substr(movie.ctitle, 1, 1) AS l, COUNT(*) as n FROM movie, media WHERE media.movie_id=movie.id GROUP BY l")
        rows = self.cursor.fetchall()
        num_09 = 0; d = {}
        for item in rows:
            o = ord(item['l'])
            if o>=ord('0') and o<=ord('9'): num_09 += item['n']
            else:
                d[item['l']] = item['n']
        d['09'] = num_09
        return d

    def get_num_movie_by_cat(self, genres):
        nums = {'genre':{}}
        for g in genres:
            self.cursor.execute("SELECT COUNT(DISTINCT movie.id) as n FROM movie, media WHERE media.movie_id=movie.id AND movie.genre LIKE '%{0}%'".format(g))
            row = self.cursor.fetchone()
            nums['genre'][g] = row['n']
        # imdb top250
        self.cursor.execute("SELECT COUNT(DISTINCT movie.id) as n FROM movie, media WHERE media.movie_id=movie.id AND movie.top250>0")
        row= self.cursor.fetchone()
        nums['Top250'] = row['n']    
        return nums
