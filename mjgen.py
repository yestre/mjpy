import re
from jinja2 import Template
from jinja2 import Environment, FileSystemLoader
from PIL import Image
from mjdb import MJDB
from imgproc import make_effect

genres = ['Action', 'Animation', 'Comedy', 'Documentary', 'Drama', 'Family', 'Fantasy', 'Sci-Fi', 'Thriller']

class JukeboxGen:
    def __init__(self, **config):
        self.dbapi = config['dbapi']
        self.src_prefix = config['src_prefix']  # path prefix for source
        self.dst_prefix = config['dst_prefix']  # path prefix for destination, to replace source prefix
        self.mjdata_path = config['mjdata_path']
        self.env = Environment(loader=FileSystemLoader('templates'))

    def gen_all(self):
        self.gen_all_listings()  # do movie detail inside
        
    def gen_media_detail(self, m):
        m['parts'] = self.dbapi.get_media_by_movie(m['id'])
        m['genre'] = m['genre'].split(',')
        self.gen_media_play_path(m['parts'])
        self.gen_movie_play_path(m, m['parts'])
        self.gen_poster_path(m)
        self.gen_playlist(m)
        tmpl = self.env.get_template('detail.html')
        rendered = tmpl.render(m=m)
        with open("{0}/{1}.html".format(self.mjdata_path, m['id']), 'w') as f:
            f.write(rendered.encode('utf-8'))
                
    def gen_all_listings(self):
        listing = self.dbapi.get_all_movies_with_media()
        # do movie detail page first
        total = len(listing)
        for index, m in enumerate(listing):
            prev = (index+total-1)/total; next=(index+1)/total
            m['prev'] = listing[prev]['id']; m['next'] = listing[next]['id']
            self.gen_media_detail(m)

        self.num_idx = self.dbapi.get_num_movie_by_first_letter()
        self.num_cat = self.dbapi.get_num_movie_by_cat(genres)

        self.gen_paginated_listing(listing, 'Other_All')

        # letter and number indices
        listing = self.dbapi.get_movies_begin_with_numbers()
        self.gen_paginated_listing(listing, "Title_09")
        l = 'A'
        for n in range(26):
            listing = self.dbapi.get_movies_begin_with_letter(l)
            self.gen_paginated_listing(listing, 'Title_'+l)
            l = chr(ord(l)+1)
            
        # genres and tags
        for genre in genres:
            listing = self.dbapi.get_movies_with_genre(genre)
            self.gen_paginated_listing(listing, 'Genres_'+genre)
        # top250
        listing = self.dbapi.get_movies_with_top250()
        self.gen_paginated_listing(listing, 'Other_Top250')

    def gen_paginated_listing(self, listing, name_prefix):
        '''
        generate a paginated listing content (in HTML).
        '''
        page_sz = 10; row_sz = 5; num_columns = page_sz/row_sz
        total = len(listing); total_pg = -(-total/page_sz)
        #if total_pg == 0: total_pg = 1 # empty list will have 1 (empty) page
        page = 0; idx = 0
        #listing_data = {0:[]}  # indexed by page
        listing_data = {}  # indexed by page
        for m in listing:
            mm = self.dbapi.get_media_by_movie(m['id'])
            self.gen_movie_play_path(m, mm)
            self.gen_poster_path(m)
            listing_data.setdefault(page, [])
            listing_data[page].append(m)
            idx +=1
            if idx%page_sz==0: page+=1
        for page, d in listing_data.iteritems():
            self.render_listing_page(page, d, total_pg, name_prefix)
            
    def render_listing_page(self, page, items_inpage, total_page, name_prefix):
        '''
        @page: the current page no, starting from 0
        @items_inpage: items in a page.
        @total_pg: total number of pages
        @name_prefix: prefix of the generated HTML file name.
        '''
        next_page = (page+1)%total_page+1
        prev_page = (page+total_page-1) % total_page + 1
        tmpl = self.env.get_template('listing.html')
        rendered = tmpl.render(next_page=next_page, prev_page=prev_page, cur_page=page+1, total_pages=total_page,
                               name_prefix=name_prefix, listing=items_inpage, num_idx=self.num_idx, num_cat=self.num_cat) 
        with open("{0}/{1}_{2}.html".format(self.mjdata_path, name_prefix, page+1), 'w') as f:
            f.write(rendered.encode('utf-8'))

    def gen_movie_play_path(self, m, media_parts):
        '''
        return the final path name for the movie to be played on the jukebox.
        For single-part movie, generate the media file path; for multi-part movie, generate the playlist
        .jsp file.
        '''
        #mm = self.dbapi.get_media_by_movie(mdata['id'])
        if len(media_parts)>1:
            m['playlist'] = m['id']+'.playlist.jsp'
        else:
            m['play_path'] = media_parts[0]['src_path'].replace(self.src_prefix, self.dst_prefix, 1)

    def gen_media_play_path(self, parts):
        for part in parts:
            part['play_path'] = part['src_path'].replace(self.src_prefix, self.dst_prefix, 1)

    def gen_poster_path(self, mdata):
        mdata['small_poster'] = mdata['id']+"_small.png"
        mdata['large_poster'] = mdata['id']+"_large.png"

    def gen_posters(self):
        '''generate all poster images'''
        movies = self.dbapi.get_all_movies_with_media()
        print "Generating poster images ..."
        for m in movies:
            self.gen_poster(m['id'], m['image_path'])

    def gen_poster(self, mid, img_path):
        shrink = 0.03; reflect = 0.12; bgcolor = "#050505"   #"#909090"
        print "    processing", img_path
        img = Image.open(img_path)
        img.thumbnail((171, 1000), Image.ANTIALIAS)
        img = make_effect(img, amount=reflect, bgcolor=bgcolor, shrink=shrink)
        ofile = "{0}/{1}_small.png".format(self.mjdata_path, mid)
        img.save(ofile)
        
        img = Image.open(img_path)
        img.thumbnail((400, 10000), Image.ANTIALIAS)
        img = make_effect(img, amount=reflect, bgcolor=bgcolor, shrink=shrink)
        ofile = "{0}/{1}_large.png".format(self.mjdata_path, mid)
        img.save(ofile)

    def gen_playlist(self, m):
        play_paths = [p['play_path'] for p in m['parts']]
        content = []
        for i, pp in enumerate(play_paths):
            # wild china 1|0|0|file:///...
            content.append("{0} {1}|0|0|{2}|".format(m['title'].encode('utf-8'), i+1, pp.encode('utf-8')))

        with open("{0}/{1}.playlist.jsp".format(self.mjdata_path, m['id']), 'w') as f:
            for line in content:
                print >>f, line

        for i in range(len(play_paths)):
            with open("{0}/{1}.playlist{2}.jsp".format(self.mjdata_path, m['id'], i+1), 'w') as f:
                for line in content:
                    print >>f, line
            content = content[1:]+content[:1]
