CREATE TABLE movie (
       id    	TEXT PRIMARY KEY,     -- imdb id or some other id
       title    TEXT,
       ctitle   TEXT,                 -- cleaned title, with stopwords removed
       desc     TEXT,
       type     INTEGER DEFAULT 0,    -- 0: movie; 1: TV program
       genre    TEXT,
       prodco   TEXT,       -- production company
       rating   TEXT,       -- IMDB rating; REAL might introduce errors
       top250   INTEGER,
       mpaa	TEXT,
       duration INTEGER,    -- in minutes
       image_path  TEXT     -- file to store the image 
);

CREATE TABLE tv_episode (
       season 	INTEGER DEFAULT 1,
       epno  	INTEGER,   -- episode no. within the series
       title    TEXT,
       desc     TEXT,
       parent   TEXT        -- parent movie id
);

CREATE TABLE media (
       src_path     TEXT,
       dst_path     TEXT,
       movie_id     TEXT,       -- id field of movie table
       episode_id   INTEGER,    -- if it is an episode. None means not an episode. rowid of tv_episode table
       system	    TEXT,
       duration	    INTEGER,
       video	    TEXT,
       width	    INTEGER,
       height	    INTEGER,
       fps	    INTEGER,
       audio	    TEXT,
       channels	    INTEGER,	-- audio channels
       UNIQUE (src_path)
);

CREATE UNIQUE INDEX IF NOT EXISTS tv_ep_idx ON tv_episode (season, epno, parent);
CREATE INDEX IF NOT EXISTS movie_ctitle_idx ON movie (ctitle);
