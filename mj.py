'''
A python mediajukebox compatible with YAMJ
'''
import subprocess
import re
import os
import os.path
import requests
from imdb import IMDB
from mjdb import MJDB
from mjgen import JukeboxGen

media_file_suffix = ['mp4','mkv', 'rm', 'avi', 'wmv']

class FileNameExtractor:
    def extract_info_in_path(self, file):
        '''
        Extract name text and other information from a file. Such info will be used to guess the canonical 
        name of the movie.

        file:   the full pathname of a file
        '''
        path = os.path.basename(file)
        # remove the suffix
        comps = path.rsplit('.', 1)
        if len(comps)==2 and comps[1] in media_file_suffix:
            path = comps[0]
        # split the words
        comps = re.findall(r"[\w']+", path)

        # find "resolution", "year", "episode" components
        ridx, resolution = self.find_resolution(comps)
        yidx, year = self.find_year(comps)
        eidx, episode = self.find_episode(comps)

        last_idx = len(comps)
        for idx in [ridx, yidx, eidx]:
            if idx>0 and idx<last_idx:
                last_idx = idx
        return { 'txt': ' '.join(comps[:last_idx]), 'year': year, 'episode': episode, 'res': resolution }

    def find_resolution(self, comps):
        idx = 0; res = None
        for c in comps:
            if re.match('720[pP]|1080[pP]', c):
                res = c
                break
            idx += 1
        if not res: idx=-1
        return idx, res

    def find_year(self, comps):
        year = None; idx = 0
        for c in comps:
            if c.isdigit() and int(c)>=1900 and int(c)<2100:
                year = int(c)
                break
            idx += 1
        if not year: idx=-1
        return idx, year

    ep_pat = [ re.compile(r'S(?P<season>\d+)E(?P<ep>\d+)'), re.compile(r'(?P<season>\d+)x(?P<ep>\d+)'), re.compile(r'Part(?P<ep>\d+)', re.I)]
    def find_episode(self, comps):
        s = None; e = None; idx = 0; matched = False
        for c in comps:
            for pat in self.ep_pat:
                m = pat.match(c)
                if m:
                    s = int(m.group('season')) if m.groupdict().has_key('season') else 1
                    e = int(m.group('ep'))
                    matched = True
                    break
            if matched: break
            idx += 1
        if not matched: idx = -1
        return idx, (s,e)

class MediaInfo:
    Lookup = { 'General': { 'Format': 'system', 'Duration': 'duration' },
               'Video': { 'Format': 'video', 'Width': 'width', 'Height': 'height', 'Frame rate': 'fps' },
               'Audio': { 'Format': 'audio', 'Channel count': 'channels' } }
    def get_info(self, pathname):
        '''
        get media info by invoking mediainfo command line.
        '''
        mdinfo = subprocess.check_output(["mediainfo", pathname])
        section = None; info = {}
        for line in mdinfo.splitlines():
            fld = [e.strip() for e in line.split(':')]
            if len(fld)==1 and self.Lookup.has_key(fld[0]):
                section = fld[0]
            elif len(fld)==2:
                k,v = fld[0].strip(), fld[1].strip()
                if section and self.Lookup[section].has_key(k):
                    info[self.Lookup[section][k]] = v
        
        return self.process_info(info)
    
    def process_pixels(self, val):
        '''1 280 pixel'''
        i = val.find('pixel')
        return int(val[:i].replace(' ',''))

    def process_fps(self, val):
        ''' 25.000 fps'''
        i = val.find('fps')
        return int(float(val[:i].replace(' ','')))

    def process_duration(self, val):
        '''1h 57m'''
        m = re.search(r'((?P<hour>\d+)h )?(?P<min>\d+)mn', val)
        if m:
            h = m.group('hour')
            h = 0 if not h else int(h)
            return h*60+int(m.group('min'))
        else:
            return 0

    def process_chan(self, val):
        i = val.find('channel')
        return int(val[:i].replace(' ',''))

    fld_processors = { 'width': process_pixels, 'height': process_pixels, 'fps': process_fps,
                       'duration': process_duration, 'channels': process_chan }
    def process_info(self, media):
        m = {}
        for k,v in media.iteritems():
            if self.fld_processors.has_key(k):
                proc = self.fld_processors[k]
                m[k] = proc(self, v)
            else:
                m[k] = v
        return m

class MediaJukebox:
    '''
    Main class for the Jukebox.
    '''
    def __init__(self, dbdir):
        '''
        @dbdir: the place to save all metadata incl. the db
        '''
        self.dbdir = dbdir      
        self.imdb = IMDB()      
        self.dbapi = MJDB(self.dbdir)
        self.fn_ext = FileNameExtractor()
        self.media_info_api = MediaInfo()

    def scan(self, indir):
        '''
        Do all files under indir.
        '''
        for root, dirs, files in os.walk(indir):
            for file in files:
                # skip unwanted files
                if file.startswith("."):
                    continue
                # find the file suffix
                suffix = file.rsplit('.',1)
                if len(suffix)!=2: 
                    continue
                suffix = suffix[-1]
                if suffix not in media_file_suffix:
                    continue
                path = os.path.abspath(root+"/"+file)
                self.do1file(path)
    
    def do1file(self, file, force=False, mid=None):
        '''
        do single file.

        force:  redo the processing, regardless if data exists already in DB.
        mid:    movie id. if provided, use it instead of guessing.
        '''
        file = os.path.abspath(file)
        print "##### working on file", file, "#####"
        media = self.dbapi.get_media(file)
        if media and not force:  # media exists
            print "file processed before. existing ..."
            return
        info = self.fn_ext.extract_info_in_path(file)
        print 'info extracted from the file name:', info
        if not mid:
            imdbid = self.imdb.search(info['txt'], info['year'])
            if not imdbid:
                print "IMDB entry not found"
            # nevertheless add an entry for the failed file
            else:
                print "IMDB: ", imdbid
                mid = "imdb-"+imdbid

        if mid:
            try:
                self.do_imdb(mid)
            except:
                print "do_imdb() failed, skipping ..."

        media_data = { 'src_path': file, 'movie_id': mid } 
        imdbid = mid.split('-')[1] if mid else None
        if imdbid:
            # if TV need to do episode
            rowid = None
            if self.dbapi.get_movie(mid, 'type')==1:  #'TV'
                season, ep = info['episode']
                if season and ep:
                    rowid = self.dbapi.get_episode(mid, season, ep)
                    if not rowid:
                        # get episode info from imdb
                        ep_data = self.imdb.get_ep_data(imdbid, season)
                        for epno, v in ep_data.iteritems():
                            print "adding season {0} episode {1} info to database ...".format(season, epno)
                            rowid = self.dbapi.add_episode({'parent':mid, 'season':season, 'epno':epno, 'title': v['title']})
                        rowid = self.dbapi.get_episode(mid, season, ep)

            # add file to media table
            media_data = { 'src_path': file, 'movie_id': mid, 'episode_id': rowid }
            media_info = self.media_info_api.get_info(file)
            media_data.update(media_info)

        self.dbapi.upsert_media(media_data)
        print "media table entry for file updated"

    def get_image_from_url(self, path, url):
        fo = open(path, 'w')
        r = requests.get(url)
        fo.write(r.content)
        fo.close()

    def do_imdb(self, mid, overwrite=False):
        imdbid = mid.split("-")[1]
        md = self.imdb.get_metadata(imdbid)
        md['id'] = mid

        # get poster
        try:
            path = os.path.abspath("{0}/posters/{1}".format(self.dbdir, md['id']))
            self.imdb.get_poster(imdbid, path)
            md['image_path'] = path
        except:
            print "get_poster() failed ..."

        # add movie
        try:
            self.dbapi.add_movie(md)
            print "movie entry for {0} added in DB".format(mid)
        except:
            if overwrite:
                self.dbapi.upd_movie(md)
                print "movie entry for {0} updated".format(mid)

    def fix_movie(self, mid, fld, val):
        '''fix movie record in the database.
        mid:    movie id.
        fld:    field name.
        val:    field value.
        '''
        md = { 'id': mid }
        if fld == "image_path":
            path = os.path.abspath("{0}/posters/{1}".format(self.dbdir, mid))
            self.get_image_from_url(path, val)
            md['image_path'] = path
        else:
            md[fld] = val
        self.dbapi.upd_movie(md)
        print "movie entry for {0} updated".format(mid)

    def fix_media(self, spath, mid=None, season=None, ep=None):
        '''
        redo the parsing of spath and fix the corresponding imdb. If provided imdb id,
        we'll use that to populate/fix the DB.

        spath:  media file source path, used as key to look up the media entry in DB.
        mid:    movie id.
        season: season id (starting from 1). Optional.
        ep:     episode id (starting from 1). Optional.
        '''
        print "##### fixing media data for file", spath, "#####"
        media = self.dbapi.get_media(spath)
        if not media:  # media exists
            print "media doesn't exist. existing ..."
            return

        info = self.fn_ext.extract_info_in_path(spath)
        print 'info extracted from the file name:', info
        if mid:
            imdbid = mid.split('-')[1]
        else:
            imdbid = self.imdb.search(info['txt'], info['year'])
            if not imdbid:
                print "IMDB entry not found, existing ..."
                return
            print "IMDB: ", imdbid
            mid = "imdb-"+imdbid
        try:
            self.do_imdb(mid)
        except:
            print "do_imdb() failed, exiting ..."
            return

        #media_data = { 'src_path': file, 'movie_id': mid } 
        # if TV need to do episode
        rowid = None
        if self.dbapi.get_movie(mid, 'type')==1:  #'TV'
            if not season or not ep:
                season, ep = info['episode']
            if season and ep:
                rowid = self.dbapi.get_episode(mid, season, ep)
                if not rowid:
                    # get episode info from imdb
                    ep_data = self.imdb.get_ep_data(imdbid, season)
                    for epno, v in ep_data.iteritems():
                        print "adding season {0} episode {1} info to database ...".format(season, epno)
                        rowid = self.dbapi.add_episode({'parent':mid, 'season':season, 'epno':epno, 'title': v['title']})

        # add file to media table
        media_data = { 'src_path': spath, 'movie_id': mid, 'episode_id': rowid }
        self.dbapi.upsert_media(media_data)
        print "media table entry for file updated"

    def fix_mediainfo(self, spath):
        media_info = self.media_info_api.get_info(spath)
        media_info['src_path'] = spath
        print media_info
        self.dbapi.upsert_media(media_info)
        print "media info updated"

    def gen(self, poster='skip', html=False, **config):
        '''
        jukebox code generation entry point.
        
        poster: can be either "skip" -- no poster image generation, or 'all'-- genreate all poster images, or <movie-id>,
                only generate poster image for that movie id.
        html: generate html code.
        config: 
            sprefix: the prefix for source path.
            dprefix: the prefix for dstination path.
            odir: directory to dump genreated code into.
        '''
        mjgen = JukeboxGen(dbapi=self.dbapi, **config)
        # gen posters
        if poster=='all':
            mjgen.gen_posters()
        elif poster != 'skip':  # treat it as mid:
            img_path = self.dbapi.get_movie(poster, 'image_path')
            if img_path:
                mjgen.gen_poster(poster, img_path)
            else:
                print "movie", poster, "not found."
        # gen html files
        if html:
            mjgen.gen_all_listings()

if __name__ == '__main__':
    import argparse
    
    # create the top-level parser
    parser = argparse.ArgumentParser(prog='Media Jukebox in Python')
    #parser.add_argument('--foo', action='store_true', help='foo help')
    
    subparsers = parser.add_subparsers(dest='action', help='sub-command help')

    # create the parser for the "scan" command
    parser_a = subparsers.add_parser('scan', help='scan a directory and build database')
    parser_a.add_argument('--indir', required=True, help='the directory from which to scan the files')

    # create the parser for the "single" command
    parser_b = subparsers.add_parser('single', help='do a single media file')
    parser_b.add_argument('--file', required=True, help='media file')
    parser_b.add_argument('--force', action="store_true", help='force update')
    parser_b.add_argument('--id', help='id, such as imdb id')

    # create the parser for the "db" command
    parser_b = subparsers.add_parser('db', help='perform db operation')
    parser_b.add_argument('--op', required=True, help='addmovie; fixmovie; fixmedia')
    parser_b.add_argument('--id', help='id, such as imdb id')
    parser_b.add_argument('--season', type=int, help='season for episode')
    parser_b.add_argument('--ep', type=int, help='episode no.')
    parser_b.add_argument('--spath', help='source path for fixmedia')
    parser_b.add_argument('--fld', help='field name (fixmovie)')
    parser_b.add_argument('--val', help='value for the field (fixmovie)')

    # create the parser for the "gen" command
    parser_b = subparsers.add_parser('gen', help='generate jukebox data')
    parser_b.add_argument('--odir', required=True, help='output directory to hold jukebox data')
    parser_b.add_argument('--sprefix', help='prefix for media source pathname')
    parser_b.add_argument('--dprefix', help='prefix for media destination pathname')    
    parser_b.add_argument('--poster', default='skip', help='poster option, all, skip, or movie-id')    
    parser_b.add_argument('--html', action='store_true', help='generate HTML code or not')    

    args = parser.parse_args()
    mj = MediaJukebox('mjdb')
    if args.action == 'scan' and args.indir:
        mj.scan(args.indir)
    elif args.action == 'gen' and args.odir:
        mj.gen(poster=args.poster, html=args.html, mjdata_path=args.odir, src_prefix=args.sprefix, dst_prefix=args.dprefix)
    elif args.action == 'single' and args.file:
        mj.do1file(args.file, args.force, args.id)
    elif args.action == 'db':
        if args.op == 'addmovie' and args.id:
            mj.do_imdb(args.id, overwrite=True)
        elif args.op == 'fixmovie' and args.id and args.fld and args.val:
            mj.fix_movie(args.id, args.fld, args.val)
        elif args.op == 'fixmedia' and args.spath:
            mj.fix_media(args.spath, args.id, args.season, args.ep)
        elif args.op == 'fixmediainfo' and args.spath:
            mj.fix_mediainfo(args.spath)
