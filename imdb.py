import requests
import lxml.html
import re

class IMDB:
    def __init__(self):
        pass
        
    def search(self, title, year):
        '''
        find the imdb id by title.
        '''
        q = '+'.join(title.split())
        r = requests.get("http://www.imdb.com/xml/find?json=1&nr=1&tt=on&q={0}".format(q))
        try:
            d = r.json()
        except:
            return None

        candidates = []
        if d.has_key('title_exact'):
            e = d['title_exact'][0]
            if e['description'][:4].isdigit():
                yy = int(e['description'][:4])
                if year and yy == year:
                    return e['id']
                else:
                    candidates.append({'id':e['id'], 'title': e['title'], 'desc': re.sub(r'<[^>]*>', '', e['title_description'])})
        if d.has_key('title_popular'):
            e = d['title_popular'][0]
            if e['description'][:4].isdigit():
                yy = int(e['description'][:4])
                if year and yy == year:
                    return e['id']
                else:
                    candidates.append({'id':e['id'], 'title': e['title'], 'desc':re.sub(r'<[^>]*>', '', e['title_description'])})
        if d.has_key('title_approx'):
            for e in d['title_approx'][:5]:
                candidates.append({'id':e['id'], 'title': e['title'], 'desc': re.sub(r'<[^>]*>', '', e['title_description'])})

        idx = 0
        for c in candidates:
            print "{0}: {1}, {2},   {3}".format(idx, c['id'], c['title'], c['desc'])
            idx += 1
        input = raw_input("Choose one that matches the media, -1 for none: ")
        input = int(input)
        if input == -1:
            return None
        return candidates[input]['id']

    def get_metadata(self, imdbid):
        '''
        get movie meta data by imdb id.
        '''
        page = requests.get("http://www.imdb.com/title/{0}/".format(imdbid))
        html = lxml.html.fromstring(page.text)

        #title
        title = html.xpath("//head/title")[0].text
        suffix = title.rfind('- IMDb')
        if suffix >= 0:
            title = title[:suffix] 
        # movie or TV series
        tvseries = html.xpath('//div[@class="infobar"]/text()')[0]
        type = 1 if 'TV Series' in tvseries else 0
        # description
        try:
            desc = html.xpath('//*[@id="overview-top"]//*[@itemprop="description"]')[0].text
            desc = desc.strip()
        except:
            desc = None
        # rating
        rating = html.xpath('//*[@id="overview-top"]//*[@itemprop="ratingValue"]')[0].text
        # top 250
        top250 = html.xpath('//*[@id="titleAwardsRanks"]//text()[contains(., "Top 250")]')
        if top250:
            top250 = top250[0]
            top250 = int(top250[top250.find('#')+1:])
        else:
            top250 = None
        #genres
        genre = html.xpath('//*[@itemprop="genre"]//a')
        genre = [n.text.strip() for n in genre]
        genre = ','.join(genre)
        # MPAA
        mpaa = html.xpath('//*[@class="infobar"]//*[@itemprop="contentRating"]')
        try:
            mpaa = mpaa[0].get('content')
        except:
            mpaa = 'NR'  # can't find 
        # duration
        duration = html.xpath('//*[@class="infobar"]//*[@itemprop="duration"]/text()')
        duration = None if not duration else int(duration[0].split()[0])
        # creator
        try:
            creator = html.xpath('//*[@itemprop="creator" and @itemtype="http://schema.org/Organization"]//*[@itemprop="name"]/text()')
            creator = creator[0].strip()
        except:
            creator = None

        md = { 'title':title.strip(), 
               'desc': desc,
               'rating': rating, 
               'top250': top250, 
               'genre': genre,
               'mpaa': mpaa,
               'duration': duration,
               'prodco': creator,
               'type': type }
        return md

    def get_poster(self, imdbid, path):
        '''
        get movie's poster.
        @path: the file pathname to write the image content to.
        '''
        page = requests.get("http://www.imdb.com/title/{0}/".format(imdbid))
        html = lxml.html.fromstring(page.text)
        try:
            # get to the page that is the image container
            img_page_url = html.xpath('//*[@id="img_primary"]//a')[0].get('href')
            if img_page_url.startswith("/"):
                img_page_url = "http://www.imdb.com"+img_page_url
            img_page = requests.get(img_page_url)
            html = lxml.html.fromstring(img_page.text)
            poster_url = html.xpath('//img[@id="primary-img"]')[0].get('src')

            fo = open(path, 'w')
            r = requests.get(poster_url)
            fo.write(r.content)
            fo.close()
            return poster_url
        except:
            print "error retrieve poster image ..."
            return None

    def get_ep_data(self, imdbid, season):
        page = requests.get("http://www.imdb.com/title/{0}/episodes?season={1}".format(imdbid, season))
        html = lxml.html.fromstring(page.text)

        episodes = html.xpath('//div[@itemprop="episodes"]')
        epdata = {}
        for ep in episodes:
            epno = ep.xpath('./meta[@itemprop="episodeNumber"]')
            if epno:
                epno = epno[0].get('content')
            else:
                continue
            title = ep.xpath('.//a[@itemprop="name"]/text()')
            title = title[0] if title else None
            epdata[int(epno)] = { 'title':  title }
        return epdata

if __name__ == '__main__':
    import sys
    
    imdb = IMDB()
    #title = ' '.join(sys.argv[1:])
    #imdbid = imdb.search(title)
    #print "id={0}".format(imdbid)
    #md = imdb.get_metadata(imdbid)
    #print md
    #print "poster url:", imdb.get_poster(imdbid, 'poster_img')
    imdbid = sys.argv[1]
    season = sys.argv[2]
    epdata = imdb.get_ep_data(imdbid, season)
    print epdata

    
